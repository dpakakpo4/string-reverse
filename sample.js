// entrée: DENIS-PAPIN KODJOVI AKAKPO
// sortie: OPKAKA IVOJDOK NIPAP-SIDEN


function reverseString(str = "") {
    let enter = [];
    let out = []

    for (var i = str.length - 1; i >= 0; i--) {
        enter.push(str.codePointAt(i));
        out[str.length - i - 1] = String.fromCodePoint(enter[str.length - i - 1])
    }
    return out.join('')
}


const reverseEs6Syntax = str => [...str].reverse().join(''); 

console.clear();

console.log(reverseEs6Syntax("DENIS 💜💜 ✡ ⭐ 𝌆 denis"));
console.log(reverseString("DENIS  ✡ ⭐ 𝌆 denis"));
console.log(reverseString("DENIS-PAPIN KODJOVI AKAKPO"));
// i have to check surrogate pairs
// console.log("💜".codePointAt(0));
// console.log(toUTF16(119558));
// console.log("𝌆".charCodeAt(0));